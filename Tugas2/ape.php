<?php

require("ape.php");

class ape{
    public $name;
    public $legs;
    public $cold_blooded;
    public $yell = "Auooo";

    public function __construct($name,$legs=4,$cold_blooded="no"){
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    
}
?>